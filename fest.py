from treelib import Node, Tree, exceptions
import os
import sys

class Tasks():
    def __init__(self, file_type, task_id, user_id):
        self.file_type = file_type
        self.task_id = task_id
        self.user_id = user_id


def scanner(path, ftree, name = None):
    size = os.stat(os.path.abspath(path)).st_size
    time = os.stat(os.path.abspath(path)).st_mtime
    file_type = 'dir' if os.path.isdir(path) else 'file'
    data = Tasks(file_type, size, time)
    if ftree.size():
        tpath = os.path.normpath(path)
        tpath = os.path.dirname(tpath)
        trash, parent = os.path.split(tpath)
        ftree.create_node(name, name, parent = parent, data = data)
    else:
        path = os.path.normpath(path)
        trash, parent = os.path.split(path)
        ftree.create_node(parent, parent, data = data)
    if file_type == 'dir':
        for obj in os.scandir(path):
            scanner(os.path.join(path, obj.name), ftree, obj.name)

ftree = Tree()
path = sys.argv[1]
scanner(path, ftree)

ftree.show()

print([ftree[node].tag for node in \
        ftree.expand_tree(filter = lambda obj: \
        obj.data.task_id > 40)])

print(['name:%s,size:%s' % (node, ftree[node].data.task_id) for node in ftree.expand_tree()])
